# Sólheimajökull GIS Project (v3) 
### CRS: WGS84 / EPSG 4326
### v3/SOLO.qgz




# ARCHIVED: Sólheimajökull GIS Project
CRS: Lambert 2004 / ISN 2004 / EPSG:5325
## Creating a new IFS qGIS Project:
1. Create a new project with the "ISN2004/Lambert2004 (EPSG:5325)" coordinate reference system.
2. Create a transform from WGS84 (or another CRS depending on the base layer imagery to be used) to ISN2004.
3. Create a base layer of imagery (opentopo, arcgis, google).
4. Set base map to ISN2004.


## Creating a new georeferenced image:
1. Create a new project, transformation(s), and base layer(s) as above.
2. Add opacity sliders to each of the layers. 
3. Raster -> Georeferencer; open raster, choose the image you are going to georeference
4. Go back and forth between the image and the aerial layer identifying common features; all around the perimeter and a sprinkling throughout the center. Choose places that are likely to have avoided the effects of wind and water. 
5. Polynomial 3; cubic spline; save GCP points

## Sources
- [Late Holocene Glacial History of Sólheimajökull, Southern Iceland (Friis)](https://drive.google.com/file/d/1hKbY0Ssca3EJD5l6LvdGbhDDe4Y3EgyW/view?usp=sharing)
- [Tephrochronology, Environmental Change and the
Norse Settlement of Iceland (Dugmore)](https://drive.google.com/file/d/1LqkM4Dyt5jJFsg6Ir10zgWm-eIpAc5qA/view?usp=sharing)
## v3 - Current
Make sure that the project, baselayers, geo-rasters, points, and lines are all based in Lambert for accuracy.

## v2
Has some useful bits but was created using the wrong CRS for certain things.

